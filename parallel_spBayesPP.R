library(parallel)
detectCores()
library(ncdf4)
library('maptools')
library(rworldmap)
library(gtools)
library(maps)
library(geoR)
library(spBayes)
library(fields)
library(maptools)
setwd("~/projectupdated/")
library(geoR)
#### Libraries ####
libs = c('chron', 'RColorBrewer', 'lattice', 'ncdf4', 'raster', 'rgdal', 'rasterVis', 'vocc', 'colorRamps', 'car',
         'doBy', 'ggplot2', 'quantreg', 'GGally', 'reshape', 'gridExtra', 'sp', 'rgeos', 'latticeExtra', 'plyr', 'DescTools', 'EnvCpt')
sapply(libs, require, character.only = TRUE)
library(dplyr)
library(spBayes)

total = read.csv("Russia34wPrecip.csv")[,-1]
# maps::map("world", lwd = 2)
# points(total$Longitude, total$Latitude, pch=15, cex = 0.2, col = alpha("red", 0.07))
# subtot = sample_n(total, 1000)
# points(subtot$Longitude, subtot$Latitude, pch=15, cex = 0.4, col = alpha("yellow", 1))
# maps::map("world", lwd = 2, add = T)

length(total$Latitude)

total$cover_fact = as.factor(total$cover_type)
total$logLOS = log(total$LOS)
names(total) = c("lon", "lat", "t", "temperature", "LOS", "ppt", "cover_type", "cover_factor")

time_dataset = total %>% group_by(t)
time.split = group_split(time_dataset)
time.split.df = lapply(time.split, data.frame)
(n.loc = length(time.split))

# knots (where to predict)
grid <- as.matrix(expand.grid(seq(50.25, 134.75, l=10), seq(40.25, 69.75, l=10)))

# land only
data(wrld_simpl)
pts <- SpatialPoints(grid, proj4string=CRS(proj4string(wrld_simpl)))
knots <- pts[!is.na(over(pts, wrld_simpl)$FIPS)]@coords

### begin function to return spatial estimates ####
spModPP = function(specify_year, formula, mcmc.iters = 1000, time.split.df, use.knots = F, knots){
  # specify_year - the year you want to get spatial
  # mcmc.iters - the number of mcmc iterations 
  # time.split.df - a list of observations separated by year
  year = specify_year - 1983
  time.n = time.split.df[[year]]
  if(use.knots == T){
    subset.sample = dim(time.split.df[[1]])[1]
  }
  else{
    subset.sample = 1000
  }
  oneyear_random = sample_n(time.n, subset.sample)
  y = oneyear_random$LOS # response
  temp = oneyear_random$temperature
  loc = cbind(oneyear_random$lon, oneyear_random$lat)
  sub.dat = data.frame(y, temp)
  # Setting existing coordinate as lat-long system
  cord.dec = SpatialPoints(cbind(oneyear_random$lon, -oneyear_random$lat), proj4string = CRS("+proj=longlat"))
  
  # Transforming coordinate to UTM using EPSG=32748 for WGS=84, UTM Zone=48M,
  # Southern Hemisphere)
  cord.UTM <- spTransform(cord.dec, CRS("+init=epsg:32748"))
  cord.UTM.df = as.data.frame(cord.UTM, xy = T)
  loc.UTM = as.matrix(cord.UTM.df)
  colnames(loc.UTM) =  c("XUTM","YUTM")
  p = 2
  if(use.knots == T){
    los.sp2 <- spLM(formula, knots = knots, modified.pp = T,
                    data=sub.dat, coords=loc.UTM,
                    starting=list("phi"=0.5,"sigma.sq"=2000,
                                  "tau.sq"=0.01),
                    tuning=list("phi"=.9, "sigma.sq"=0.01,
                                "tau.sq"=.5),
                    priors=list("beta.Norm"=list(rep(0,p), diag(2500,p)), "phi.Unif"=c(0.01, 1000),
                                "sigma.sq.IG"=c(3, 1),
                                "tau.sq.IG"=c(2, 2)),
                    cov.model="exponential",n.samples=mcmc.iters)
  }
  else{
    los.sp2 <- spLM(formula, 
                    data=sub.dat, coords=loc.UTM,
                    starting=list("phi"=0.05,"sigma.sq"=1800,
                                  "tau.sq"=0.01),
                    tuning=list("phi"=.9, "sigma.sq"=0.01,
                                "tau.sq"=.5),
                    priors=list("beta.Norm"=list(rep(0,p), diag(2000,p)), "phi.Unif"=c(0.01, 1000),
                                "sigma.sq.IG"=c(3, 1),
                                "tau.sq.IG"=c(10, 0.1)),
                    cov.model="exponential",n.samples=mcmc.iters)
  }
  burn.in <- floor(0.75*mcmc.iters)
  # los.sp.recover2 <- spRecover(los.sp2, start=burn.in, n.report=10)
  # beta.samples.los2 = los.sp.recover2$p.beta.recover.samples
  # w.samples.los2 = los.sp.recover2$p.w.recover.samples
  
  return(los.sp2)
  # return(list(p.theta.samples = los.sp2$p.theta.samples, 
  #             beta.samples.los2 = los.sp.recover2$p.beta.recover.samples,
  #             w.samples.los2 = los.sp.recover2$p.w.recover.samples))
}

system.time(out1 <- spModPP(2007, formula = y~ temp, mcmc.iters = 500, time.split.df = time.split.df, use.knots = T, knots = knots))


# plot(out1$p.theta.samples)

# system.time(save2 <- mclapply(1:100, f))
# system.time(outSP <- mclapply(seq(1984,2017), spModPP, mcmc.iters = 10000, 
#                               formula = y ~ temp, use.knots = T, knots = knots, time.split.df = time.split.df, 
#                               mc.preschedule = F, mc.cores = 6))
# 1984+24-1
outSP[[24]] = out1


#### end ####
plotSpatialParams = function(output_spModPP){
  getSpatialParam = function(param, dlist = outSP){
    return(lapply(outSP, function(x){data.frame(x$p.theta.samples)[,param]} ))
  }
  # Boxplot all spatial params at once
  spParams = lapply(seq(1, 3 ), getSpatialParam, dlist = outSP)
  
  # do the plot
  par(mar = c(6,6,6,6))
  # par(mfrow = c(3, 1), mar = c(4, 5, 4, 3))
  boxplot(spParams[[1]], ylab = expression("Estimate for " ~ sigma^2 ), xlab = "Year", cex.lab = 2)
  expression_sp = c(expression("Estimate for " ~ tau^2 ), expression("Estimate for " ~ phi ))
  for (i in 2:3) {
    boxplot(spParams[[i]], ylab = expression_sp[i-1], xlab ="Year", cex.lab = 2)
  }
}

plotSpatialParams(outSP)
plot(outSP[[1]]$p.theta.samples[,3])


hist(outSP[[2]]$p.theta.samples[,1])
hist(outSP[[2]]$p.theta.samples[,2])

hist(tail(outSP[[2]]$p.theta.samples[,3],1000), freq = F)
# lines(density(tail(outSP[[2]]$p.theta.samples[,3],1000)))




plot(outSP[[1]]$p.theta.samples)
save(outSP, file = "outpp2.RData")

dim(outSP[[1]]$w.samples.los2)
outSP[[1]]$w.samples.los2[1,1]


get.w = summary(mcmc(t(outSP[[1]]$w.samples.los2)))$quantiles[,c(3,1,5)]
dim(get.w)
