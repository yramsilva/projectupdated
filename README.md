## MASTERS PROJECT 
## Main Scripts:
Phenology_GetVars.R: Script to merge Temperature and Length of Season(LOS) data and reshape to long format. Outputs .csv files depending on inputs

RegressionDiscussion1.R: Script to perform preliminary regression models with Temperature as covariates.

Tmax_GetVars.R: Same as Phenology_GetVars.R but with Tmax.


## Main .nc files
Tmax.nc: Raw annual maximum temperature
Precip.nc: Raw annual average precipitation 1984-2017
Soil.nc: Raw annual average soil moisture 1984-2017
EOS_World_0.5deg_QA_Smooth.nc: from Yiluan, used for analysis
LOS_World_0.5deg_QA_Smooth.nc: ''
Tmin_0.5degree.nc: ''
TMP_World.nc: ''

## Main .csv files:
Total_Tmax.csv: long format, covering entire globe, with variables:
	- Lon, Lat, Year, Max_Annual_Temperature, LOS, cover_type.

Combined34_2.csv: correct long format with variables:
	- Lon, Lat, Year, Mean_annual_Temperature, LOS, cover_type

FullData34wPrecip.csv long format, covering entire globe, with variables:
	- "Longitude"   "Latitude"    "Year"        "Temperature" "LOS"         "ppt"         "cover_type"
	

N25_34yrs_wPrecip.csv: same as FullData34wPrecip.csv but added precipitation region and above 25 degrees north latitude.

Russia34.csv: similar 

