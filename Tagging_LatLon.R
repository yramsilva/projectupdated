setwd("~/phenology/")

#### Libraries ####
libs = c('chron', 'RColorBrewer', 'lattice', 'ncdf4', 'raster', 'rgdal', 'rasterVis', 'vocc', 'colorRamps', 'car',
         'doBy', 'ggplot2', 'quantreg', 'GGally', 'reshape', 'gridExtra', 'sp', 'rgeos', 'latticeExtra', 'plyr', 'DescTools', 'EnvCpt')
sapply(libs, require, character.only = TRUE)

total = read.csv("Russia34.csv")[,-1]

total$cover_fact = as.factor(total$cover_type)
names(total) = c("lon", "lat", "t", "temperature", "LOS", "cover_type", "cover_factor")
total$LOS = log(total$LOS)
head(total)
dim(total)
summary(total)

