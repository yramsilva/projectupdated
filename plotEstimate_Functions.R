setwd("~/projectupdated/")
library(geoR)
#### Libraries ####
libs = c('chron', 'RColorBrewer', 'lattice', 'ncdf4', 'raster', 'rgdal', 'rasterVis', 'vocc', 'colorRamps', 'car',
         'doBy', 'ggplot2', 'quantreg', 'GGally', 'reshape', 'gridExtra', 'sp', 'rgeos', 'latticeExtra', 'plyr', 'DescTools', 'EnvCpt')
sapply(libs, require, character.only = TRUE)
library(dplyr)
library(spBayes)






plotSpatialParams = function(output_spModPP){
  getSpatialParam = function(param, dlist = outSP){
    return(lapply(outSP, function(x){data.frame(x$p.theta.samples)[,param]} ))
  }
  # Boxplot all spatial params at once
  spParams = lapply(seq(1, 3 ), getSpatialParam, dlist = outSP)
  
  # do the plot
  par(mfrow = c(3, 1), mar = c(4, 5, 4, 3))
  boxplot(spParams[[1]], ylab = expression("Estimate for " ~ sigma^2 ), xlab = "Year", cex.lab = 2)
  expression_sp = c(expression("Estimate for " ~ tau^2 ), expression("Estimate for " ~ phi ))
  for (i in 2:3) {
    boxplot(spParams[[i]], ylab = expression_sp[i-1], xlab ="Year", cex.lab = 2)
  }
}

plotSpatialParams(outSP)

# Boxplot: get all betas at once regardless of size
plot_BetaParams = function(output_spModPP){
  getRegressParam = function(param, dlist = outSP){
    return(lapply(outSP, function(x){data.frame(x$beta.samples.los2)[,param]} ))
  }
  
  
  beta_counts = dim(outSP[[1]]$beta.samples.los2)[2] # specify how many betas in total (depends on predictors)
  betas = lapply(seq(1, beta_counts ), getRegressParam, dlist = outSP)
  par(mfrow = c(beta_counts, beta_counts/3), mar = c(4, 5, 4, 3))
  
  boxplot(betas[[1]], ylab = "Estimate for beta 0", xlab = "Year", cex.lab = 2)
  for (i in 2:beta_counts) {
    boxplot(betas[[i]], ylab = paste("Estimate for beta", i), xlab ="Year", cex.lab = 2)
  }
}
plot_BetaParams(outSP)
