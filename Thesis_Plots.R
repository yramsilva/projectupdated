require(raster)
setwd("~/MastersProjectII/")
# custom LOS
lostemp = "LOS_World_0.5deg_QA_Smooth.nc"
LOS <- brick(lostemp)
LOS_Sub = LOS
?rotate
r <- LOS_Sub[[34]]
par(mar = c(4,4,4,4))
e <- extent(50.25, 134.75, 40.25, 69.75)
r = crop(r, e)

intras <- mask(r, crop(TMP,e)) 
plot(expand.grid(seq(-100, 200, l=10), seq(-100, 200, l=10)), cex = 100, pch = 15,
     xlim = c(0,150), ylim = c(0,70), col = "lightgrey", cex.lab = 1.5,
     xlab = "Longitude", ylab = "Latitude", main=paste("LOS Subset (2014)"), axes = F, cex.main = 1.5)
map("world", lwd =2, add = T, fill = T, col = "white")
plot(intras, col=terrain.colors(20), legend=FALSE, axes=F,xlim = c(-168,180), add = T)
map("world", lwd =2, add = T)
axis(1, cex.axis = 1.5)
axis(2, cex.axis = 1.5)
box()
r.range <- c(1, 365)
# plot(r, legend.only=TRUE, col=terrain.colors(20),
#      legend.width=1.5, legend.shrink=0.75,
#      axis.args=list(at=seq(r.range[1], r.range[2], 30),
#                     labels=seq(r.range[1], r.range[2], 30), 
#                     cex.axis=1.5),
#      legend.args=list(text="LOS (Days)", side=4, font=2, line=-3, cex=1.5))
plot(wrld_simpl, add = T)

title("LOS Subset (2014)", cex.main = 2.5, line = 1.)

# custom SOS
lostemp = "SOS_World_0.5deg_QA_Smooth.nc"
LOS <- brick(lostemp)
LOS_Sub = LOS

r <- LOS_Sub[[34]]
par(mar = c(4,4,4,8))


intras <- mask(r, TMP) 
plot(intras, col=terrain.colors(100), legend=FALSE, axes=F,xlim = c(-168,180))
axis(1, cex.axis = 1.5)
axis(2, cex.axis = 1.5)

r.range <- c(1, 365)
plot(r, legend.only=TRUE, col=terrain.colors(100),
     legend.width=1.5, legend.shrink=0.75,
     axis.args=list(at=seq(r.range[1], r.range[2], 30),
                    labels=seq(r.range[1], r.range[2], 30), 
                    cex.axis=1.5),
     legend.args=list(text="LOS (Days)", side=4, font=2, line=-3, cex=1.5))
plot(wrld_simpl, add = T, xlim = c(-168,180))

title("Start of Season (SOS) for 2014", cex.main = 2.5, line = 1.5)

# custom EOS
lostemp = "EOS_World_0.5deg_QA_Smooth.nc"
LOS <- brick(lostemp)
LOS_Sub = LOS

r <- LOS_Sub[[34]]
par(mar = c(4,4,4,8))


intras <- mask(r, TMP) 
plot(intras, col=terrain.colors(100), legend=FALSE, axes=F,xlim = c(-168,180))
axis(1, cex.axis = 1.5)
axis(2, cex.axis = 1.5)

r.range <- c(1, 365)
plot(r, legend.only=TRUE, col=terrain.colors(100),
     legend.width=1.5, legend.shrink=0.75,
     axis.args=list(at=seq(r.range[1], r.range[2], 30),
                    labels=seq(r.range[1], r.range[2], 30), 
                    cex.axis=1.5),
     legend.args=list(text="LOS (Days)", side=4, font=2, line=-3, cex=1.5))
plot(wrld_simpl, add = T, xlim = c(-168,180))

title("End of Season (EOS) for 2014", cex.main = 2.5, line = 1.5)


# custom Temp
lostemp = "TMP_World.nc"
LOS <- brick(lostemp)
LOS_Sub = LOS

MAT <- LOS_Sub[[34]]
par(mar = c(4,4,4,8))

r_agg <- aggregate(MAT,fact=c(2,1),fun=mean)
?aggregate
res(r_agg)
plot(r, col=colorRampPalette(c("blue", "cyan", "yellow", "orange", "red"))(100), 
     legend=FALSE, axes=F, xlim = c(-168,178))
axis(1, cex.axis = 1.5, at = seq(-150,150, length.out = 5))
axis(2, cex.axis = 1.5)

r.range <- c(-35, 35)
plot(r, legend.only=TRUE, col=colorRampPalette(c("blue", "cyan", "yellow", "orange", "red"))(100),
     legend.width=1.5, legend.shrink=0.75, 
     axis.args=list(at=seq(r.range[1], r.range[2], 6),
                    labels=seq(r.range[1], r.range[2], 6), 
                    cex.axis=1.5),
     legend.args=list(text=expression("Temperature" ~ (C^o)), side=4, font=2, line=-3, cex=1.5))

title("Mean Annual Temperature for 2014", cex.main = 2.5, line = 1.5)
map("world", lwd =2, add = T, xlim = c(-168,170))

# custom crop type 
require(raster)
setwd("~/MastersProjectII/")
file <- c("sdat_10011_1_20190321_025613752.nc")#World
cover <- brick(file)
plot(covermask)

r <- covermask
par(mar = c(4,4,4,8))


plot(r, col=colorRampPalette(c("green", "brown", "yellow", "white"))(20), legend=FALSE, axes=F)
axis(1, cex.axis = 1.5)
axis(2, cex.axis = 1.5)

r.range <- c(1, 35)
plot(r, legend.only=TRUE, col=colorRampPalette(c("green", "brown", "yellow", "white"))(20),
     legend.width=1.5, legend.shrink=0.75,
     axis.args=list(at=seq(r.range[1], r.range[2], 2),
                    labels=seq(r.range[1], r.range[2], 2), 
                    cex.axis=1.5),
     legend.args=list(text="Cover Type", side=4, font=2, line=-3, cex=1.5))

title("Global Land Cover/Crop Type", cex.main = 2.5, line = 1.5)


# subset region
plot(0,0, xlim = c(-180, 180), ylim = c(-90,90), col = "white", xlab = "", ylab = "")
map("world", lwd =1, add = T)
lon.lim = c(50.25, 134.75) 
lat.lim = c(40.25, 69.75)
h1 = seq(50.25, 134.75, length.out = 100)
v1 = seq(40.25, 69.75, length.out = 100)
lines(h1, rep(lat.lim[1],100), col = "red", lwd = 4)
lines(h1, rep(lat.lim[2],100), col = "red", lwd = 4)
lines(rep(lon.lim[1], 100), v1, col = "red", lwd = 4)
lines(rep(lon.lim[2], 100), v1, col = "red", lwd = 4)
title("Subset Region", cex.main = 2.5, line = 1.5)

# demonstrate aggregation
{
set.seed(1)
par(mfrow = c(1,2))
plot(0, 0, xlim = c(0.3,3.73), ylim = c(0.3,3.73), axes = F, xlab = "", ylab = "",
     col = "white")
lines.plots = seq(0,4)
lines.plots2 = seq(0,4, by=2)

abline(v = lines.plots, col="black", lwd = 3)
abline(h = lines.plots, col="black", lwd = 2)
abline(v = lines.plots2, col="red", lwd = 3)
abline(h = lines.plots2, col="red", lwd = 3)
box(lwd = 3, col = "red")



nums.mat = matrix(NA, ncol = 4, nrow = 4)
center = seq(0.5, 3.5)
nums.mat[1:2,1:2] = matrix(sample(10:16, 4, replace = F), ncol = 2, nrow = 2)
nums.mat[1:2,3:4] = matrix(sample(20:26, 4, replace = F), ncol = 2, nrow = 2)
nums.mat[3:4,1:2] = matrix(sample(30:38, 4, replace = F), ncol = 2, nrow = 2)
nums.mat[3:4,3:4] = matrix(sample(40:49, 4, replace = F), ncol = 2, nrow = 2)
nums.mat

text(center, rep(center[1]), nums.mat[4,], cex = 1.5)
text(center, rep(center[2]), nums.mat[3,], cex = 1.5)
text(center, rep(center[3]), nums.mat[2,], cex = 1.5)
text(center, rep(center[4]), nums.mat[1,], cex = 1.5)

center2 = c(1,3)
one = mean(nums.mat[1:2,1:2]) # 30.75
two = mean(nums.mat[3:4,1:2]) # 39
three = mean(nums.mat[1:2,3:4]) #30.75
four = mean(nums.mat[3:4,3:4])

(20+17+24+42)/4
(30+29+50+11)/4
(16+45+15+22)/4

nums.mat2 = matrix(NA, 2, 2)
nums.mat2[1,1] = one
nums.mat2[2,2] = four
nums.mat2[2,1] = two
nums.mat2[1,2] = three

plot(0, 0, xlim = c(0.3,3.73), ylim = c(0.3,3.73), axes = F, xlab = "", ylab = "",
     col = "white")
abline(v = lines.plots2, col="black",lwd = 3)
abline(h = lines.plots2, col="black",lwd = 3)
box(col = "black", lwd = 3)

text(center2, rep(center2[1]), nums.mat2[2,], cex = 2)
text(center2, rep(center2[2]), nums.mat2[1,], cex = 2)
}
title("Mean Aggregatetion", outer = T, line = -2, cex.main = 2.5)
