###################################################
### code chunk number 1: libs
###################################################
options(width = 85) 
library(spBayes)
library(MBA)
library(geoR)
library(fields)
library(dplyr)
setwd("~/private_wip/")
full_dat = read.csv("FullDatAggregate.csv")[,-1]
head(full_dat)
dim(full_dat)

#temperature component (analysis) 
TEMP.names = names(full_dat)[37:70]
temp.eda.dat = full_dat[,c(TEMP.names)]
head(temp.eda.dat)
eda.temp.range = apply(temp.eda.dat, 1, FUN=range)
dim(eda.temp.range)
diff.temp = apply(eda.temp.range, 2, diff)
# for now, since temp doesn't fluctuate too much over time i just used average temperature
full_dat$MT = apply(temp.eda.dat, 1, mean)
head(full_dat)
dim(full_dat)

dat = sample_n(full_dat, 50)



#create a vector of responses for LOS
y.t <- dat[,3:36]
head(y.t)
N.t <- ncol(y.t) ##number of years
n <- nrow(y.t) ##number of observation per years
##add some missing observations to illustrate prediction
miss <- sample(1:N.t, 15)
holdout.station.id <- 5
y.t.holdout <- y.t[holdout.station.id, miss]
y.t[holdout.station.id, miss] <- NA

coords = as.matrix(dat[,c("x","y")])
max.d = max(iDist(coords))


###################################################
### code chunk number 4: priors
###################################################
##set starting and priors
p <- 2 #number of regression parameters in each year; intercept + MT


starting <- list("beta"=rep(0,N.t*p), "phi"=rep(3/(0.5*max.d), N.t),
                 "sigma.sq"=rep(2,N.t), "tau.sq"=rep(1, N.t),
                 "sigma.eta"=diag(rep(0.01, p)))

tuning <- list("phi"=rep(0.75, N.t), "sigma.sq" = rep(0.01, N.t)) 

priors <- list("beta.0.Norm"=list(rep(0,p), diag(1000,p)),
               "phi.Unif"=list(rep(3/(0.9*max.d), N.t), rep(3/(0.05*max.d), N.t)),
               "sigma.sq.IG"=list(rep(2,N.t), rep(10,N.t)),
               "tau.sq.IG"=list(rep(2,N.t), rep(5,N.t)),
               "sigma.eta.IW"=list(2, diag(0.001,p)))

###################################################
### code chunk number 5: spDynLM
###################################################
##make symbolic model formula statement for each month
mods <- lapply(paste(colnames(y.t),'MT',sep='~'), as.formula)
n.samples <- 500

m.1 <- spDynLM(mods, data=cbind(y.t,dat[,"MT",drop=FALSE]), coords=coords,
               starting=starting, tuning=tuning, priors=priors,
               cov.model="exponential", n.samples=n.samples, n.report=50) 

plot.ts(m.1$p.beta.0.samples)
dim(m.1$p.beta.0.samples)
keep = colnames(m.1$p.theta.samples)
sig.keeps = seq(1,102, by=3)
keep[sig.keeps]

sig_quants = summary(m.1$p.theta.samples[,sig.keeps])$quantiles
reinit_sig.sq = sig_quants[,"50%"]

#### begin tuning
##reinitialize sig2 starting and priors
p <- 2 #number of regression parameters in each year

n.samples <- 1500
starting <- list("beta"=rep(0,N.t*p), "phi"=rep(3/(0.5*max.d), N.t),
                 "sigma.sq"=reinit_sig.sq, "tau.sq"=rep(10, N.t),
                 "sigma.eta"=diag(rep(100, p)))

tuning <- list("phi"=rep(0.75, N.t), "sigma.sq"=rep(0.01,N.t)) 

priors <- list("beta.0.Norm"=list(rep(0,p), diag(1000,p)),
               "phi.Unif"=list(rep(3/(0.9*max.d), N.t), rep(3/(0.05*max.d), N.t)),
               "sigma.sq.IG"=list(rep(1,N.t), rep(2,N.t)),
               "tau.sq.IG"=list(rep(2,N.t), rep(5,N.t)),
               "sigma.eta.IW"=list(2, diag(0.1,p)))

m.2 <- spDynLM(mods, data=cbind(y.t,dat[,"MT",drop=FALSE]), coords=coords,
               starting=starting, tuning=tuning, priors=priors,
               cov.model="exponential", n.samples=n.samples, n.report=100, get.fitted = T) 
n.burn = 1:500
plot.ts(m.2$p.beta.0.samples[-n.burn,])

###################################################
### code chunk number 6: beta
###################################################

quant <- function(x){quantile(x, prob=c(0.5, 0.025, 0.975))}


beta <- apply(m.1$p.beta.samples[-n.burn,], 2, quant)
beta.0 <- beta[,grep("Intercept", colnames(beta))]
beta.1 <- beta[,grep("MT", colnames(beta))]


plot(1:N.t, beta.0[1,], pch=19, cex=0.5, xlab="year", ylab="beta.0", ylim=range(beta.0))
arrows(1:N.t, beta.0[1,], 1:N.t, beta.0[3,], length=0.02, angle=90)
arrows(1:N.t, beta.0[1,], 1:N.t, beta.0[2,], length=0.02, angle=90)

plot(1:N.t, beta.1[1,], pch=19, cex=0.5, xlab="year", ylab="beta.1", ylim=range(beta.1))
arrows(1:N.t, beta.1[1,], 1:N.t, beta.1[3,], length=0.02, angle=90)
arrows(1:N.t, beta.1[1,], 1:N.t, beta.1[2,], length=0.02, angle=90)

plot.ts(m.2$p.theta.samples[-n.burn,1])
###################################################
### code chunk number 7: theta
###################################################
theta <- apply(m.2$p.theta.samples[-n.burn,], 2, quant)
sigma.sq <- theta[,grep("sigma.sq", colnames(theta))]
tau.sq <- theta[,grep("tau.sq", colnames(theta))]
phi <- theta[,grep("phi", colnames(theta))]
plot(1:N.t, sigma.sq[1,], pch=19, cex=0.5, xlab="months", ylab="sigma.sq", ylim=range(sigma.sq))
arrows(1:N.t, sigma.sq[1,], 1:N.t, sigma.sq[3,], length=0.02, angle=90)
arrows(1:N.t, sigma.sq[1,], 1:N.t, sigma.sq[2,], length=0.02, angle=90)

plot(1:N.t, tau.sq[1,], pch=19, cex=0.5, xlab="months", ylab="tau.sq", ylim=range(tau.sq))
arrows(1:N.t, tau.sq[1,], 1:N.t, tau.sq[3,], length=0.02, angle=90)
arrows(1:N.t, tau.sq[1,], 1:N.t, tau.sq[2,], length=0.02, angle=90)

plot(1:N.t, 3/phi[1,], pch=19, cex=0.5, xlab="months", ylab="eff. range (km)", ylim=range(3/phi))
arrows(1:N.t, 3/phi[1,], 1:N.t, 3/phi[3,], length=0.02, angle=90)
arrows(1:N.t, 3/phi[1,], 1:N.t, 3/phi[2,], length=0.02, angle=90)

###################################################
### code chunk number 8: pred
###################################################
y.hat <- apply(m.2$p.y.samples[,-n.burn], 1, quant)
y.hat.med <- matrix(y.hat[1,], ncol=N.t)
y.hat.up <- matrix(y.hat[3,], ncol=N.t)
y.hat.low <- matrix(y.hat[2,], ncol=N.t)

y.obs <- as.vector(as.matrix(y.t[-holdout.station.id, -miss]))
y.obs.hat.med <- as.vector(y.hat.med[-holdout.station.id, -miss])
y.obs.hat.up <- as.vector(y.hat.up[-holdout.station.id, -miss])
y.obs.hat.low <- as.vector(y.hat.low[-holdout.station.id, -miss])

y.ho <- as.matrix(y.t.holdout)
y.ho.hat.med <- as.vector(y.hat.med[holdout.station.id, miss])
y.ho.hat.up <- as.vector(y.hat.up[holdout.station.id, miss])
y.ho.hat.low <- as.vector(y.hat.low[holdout.station.id, miss])

par(mfrow=c(1,2))
plot(y.obs, y.obs.hat.med, pch=19, cex=0.5, xlab="observed", ylab="fitted", main="Observed vs. fitted",
     ylim = c(-50,300))
arrows(y.obs, y.obs.hat.med, y.obs, y.obs.hat.up, length=0.02, angle=90)
arrows(y.obs, y.obs.hat.med, y.obs, y.obs.hat.low, length=0.02, angle=90)
lines(-300:300, -300:300, col="blue", lwd=2)

plot(y.ho, y.ho.hat.med, pch=19, cex=0.5, 
     xlab="observed", ylab="predicted", main="Observed vs. predicted")
arrows(y.ho, y.ho.hat.med, y.ho, y.ho.hat.up, length=0.02, angle=90)
arrows(y.ho, y.ho.hat.med, y.ho, y.ho.hat.low, length=0.02, angle=90)
lines(-500:500, -500:500, col="blue")


